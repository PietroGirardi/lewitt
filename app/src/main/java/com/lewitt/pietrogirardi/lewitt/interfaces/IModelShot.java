package com.lewitt.pietrogirardi.lewitt.interfaces;

/**
 * Created by p.girardi on 6/30/2016.
 */

public interface IModelShot {

    void getListShotFromModel(int page);
}
