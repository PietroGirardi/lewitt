package com.lewitt.pietrogirardi.lewitt.interfaces;

import com.lewitt.pietrogirardi.lewitt.entities.Shot;

import java.util.List;

/**
 * Created by p.girardi on 6/30/2016.
 */

public interface IViewShot {
    public void onListShotResult(List<Shot> listShot);
    public void onFailure();
}
