package com.lewitt.pietrogirardi.lewitt.view;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.lewitt.pietrogirardi.lewitt.R;
import com.lewitt.pietrogirardi.lewitt.adapters.ShotAdapter;
import com.lewitt.pietrogirardi.lewitt.entities.Shot;
import com.lewitt.pietrogirardi.lewitt.interfaces.IViewShot;
import com.lewitt.pietrogirardi.lewitt.presenter.PresenterShot;
import com.peekandpop.shalskar.peekandpop.PeekAndPop;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ShotFragmentView extends Fragment implements IViewShot, ShotAdapter.OnItemClickListener {

    public static final int LAYOUT_MANAGER_LIST = 1;
    public static final int LAYOUT_MANAGER_GRID = 2;

    @BindView(R.id.main_toolbar)Toolbar toolbar;
    @BindView(R.id.main_collapsing)CollapsingToolbarLayout collapsingToolbarLayout;

    private View rootView;
    private RecyclerView rvShot;
    private SwipeRefreshLayout swipeRefreshLayout;
    private PresenterShot presenterShot;
    private ShotAdapter shotAdapter;
    private int page = 1;
    public static int layoutManagerType;
    private PeekAndPop peekAndPop;
    private int currentOrientation;
    ProgressDialog progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        createAnimation();

        //verifying to don't inflate the layout again when back from detail
        if(rootView == null) {

            progress = ProgressDialog.show(getActivity(),getString(R.string.dialog_load_title), getString(R.string.dialog_load_message));
            progress.show();
            setHasOptionsMenu(true);

            rootView = inflater.inflate(R.layout.fragment_shot, container, false);
            ButterKnife.bind(this, rootView);

            rvShot = ButterKnife.findById(rootView, R.id.rv_list);
            rvShot.setHasFixedSize(true);

            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
            collapsingToolbarLayout.setTitle("leWitt");

            //StaggeredGridLayoutManager sgl = new StaggeredGridLayoutManager(getColumnCount(), StaggeredGridLayoutManager.VERTICAL);
            GridLayoutManager glm = new GridLayoutManager(getActivity(), getColumnCount());

            int spanCount = 2; // 2 columns
            int spacing = 50; // 50px
            boolean includeEdge = true;
            rvShot.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
            rvShot.setLayoutManager(glm);

            swipeRefreshLayout = ButterKnife.findById(rootView, R.id.swipeRefreshLayout);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    // TODO: 7/5/2016 implement update new posts
                    swipeRefreshLayout.setRefreshing(false);
                }
            });

            presenterShot = new PresenterShot(this);
            presenterShot.getListShot(page);

            rvShot.addOnScrollListener(presenterShot);
        }

        if(getActivity().getResources().getConfiguration().orientation != currentOrientation){
            GridLayoutManager glm = new GridLayoutManager(getActivity(), getColumnCount());
            rvShot.setLayoutManager(glm);
        }

        this.setRetainInstance(true);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onListShotResult(List<Shot> listShot) {
        updateAdapter(listShot);
    }

    @Override
    public void onFailure() {
        progress.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.dialog_failure_message))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void updateAdapter(List<Shot> listShot){
        progress.dismiss();

        //3D touch
        peekAndPop = new PeekAndPop.Builder(getActivity())
                .blurBackground(false)
                .peekLayout(R.layout.item_3dtouch) //xml of 3D touch view
                .parentViewGroupToDisallowTouchEvents(rvShot)
                .build();


        //getting info before update list
        float topOffset = 0;
        LinearLayoutManager manager = (LinearLayoutManager) rvShot.getLayoutManager();
        int firstItem = manager.findFirstVisibleItemPosition();
        View firstItemView = manager.findViewByPosition(firstItem);
        if(firstItemView !=null)
        topOffset = firstItemView.getTop();

        //updating
        shotAdapter = new ShotAdapter(getActivity(), listShot, peekAndPop);
        shotAdapter.setRecyclerViewOnClickListenerHack((ShotAdapter.OnItemClickListener) ShotFragmentView.this);
        rvShot.setAdapter(shotAdapter);

        //setting scroll to last know position
        manager.scrollToPositionWithOffset(firstItem, (int) topOffset);

        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenterShot.onDestroy();
    }

    @Override
    public void onItemClick(Shot shot) {
        presenterShot.onItemClick(getActivity() ,shot);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id){
            case R.id.action_list:
                rvShot.setLayoutManager(new LinearLayoutManager(getActivity()));
                rvShot.setAdapter(shotAdapter);
                layoutManagerType = LAYOUT_MANAGER_LIST;
                break;
            case R.id.action_grid:
                rvShot.setLayoutManager(new GridLayoutManager(getActivity(), getColumnCount()));
                rvShot.setAdapter(shotAdapter);
                layoutManagerType = LAYOUT_MANAGER_GRID;
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    //TRANSITION ANIMATION
    private void createAnimation(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Slide trans1 = new Slide();
            trans1.setDuration(1000);

            Fade trans2 = new Fade();
            trans2.setDuration(1000);

            this.setExitTransition(trans2);
            this.setReenterTransition(trans1);
            //this.setReturnTransition(trans1);
        }
    }


    private int getColumnCount(){
       currentOrientation =  getActivity().getResources().getConfiguration().orientation;

        if(Configuration.ORIENTATION_LANDSCAPE == currentOrientation)
            return 3;

        return 2;
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}