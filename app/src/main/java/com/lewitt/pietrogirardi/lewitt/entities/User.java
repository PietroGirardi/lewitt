
package com.lewitt.pietrogirardi.lewitt.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("html_url")
    @Expose
    private String htmlUrl;
    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("links")
    @Expose
    private Links links;
    @SerializedName("buckets_count")
    @Expose
    private int bucketsCount;
    @SerializedName("comments_received_count")
    @Expose
    private int commentsReceivedCount;
    @SerializedName("followers_count")
    @Expose
    private int followersCount;
    @SerializedName("followings_count")
    @Expose
    private int followingsCount;
    @SerializedName("likes_count")
    @Expose
    private int likesCount;
    @SerializedName("likes_received_count")
    @Expose
    private int likesReceivedCount;
    @SerializedName("projects_count")
    @Expose
    private int projectsCount;
    @SerializedName("rebounds_received_count")
    @Expose
    private int reboundsReceivedCount;
    @SerializedName("shots_count")
    @Expose
    private int shotsCount;
    @SerializedName("teams_count")
    @Expose
    private int teamsCount;
    @SerializedName("can_upload_shot")
    @Expose
    private boolean canUploadShot;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("pro")
    @Expose
    private boolean pro;
    @SerializedName("buckets_url")
    @Expose
    private String bucketsUrl;
    @SerializedName("followers_url")
    @Expose
    private String followersUrl;
    @SerializedName("following_url")
    @Expose
    private String followingUrl;
    @SerializedName("likes_url")
    @Expose
    private String likesUrl;
    @SerializedName("projects_url")
    @Expose
    private String projectsUrl;
    @SerializedName("shots_url")
    @Expose
    private String shotsUrl;
    @SerializedName("teams_url")
    @Expose
    private String teamsUrl;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @param username
     *     The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * @return
     *     The htmlUrl
     */
    public String getHtmlUrl() {
        return htmlUrl;
    }

    /**
     * 
     * @param htmlUrl
     *     The html_url
     */
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    /**
     * 
     * @return
     *     The avatarUrl
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }

    /**
     * 
     * @param avatarUrl
     *     The avatar_url
     */
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    /**
     * 
     * @return
     *     The bio
     */
    public String getBio() {
        return bio;
    }

    /**
     * 
     * @param bio
     *     The bio
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     * 
     * @return
     *     The location
     */
    public String getLocation() {
        return location;
    }

    /**
     * 
     * @param location
     *     The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * 
     * @return
     *     The links
     */
    public Links getLinks() {
        return links;
    }

    /**
     * 
     * @param links
     *     The links
     */
    public void setLinks(Links links) {
        this.links = links;
    }

    /**
     * 
     * @return
     *     The bucketsCount
     */
    public int getBucketsCount() {
        return bucketsCount;
    }

    /**
     * 
     * @param bucketsCount
     *     The buckets_count
     */
    public void setBucketsCount(int bucketsCount) {
        this.bucketsCount = bucketsCount;
    }

    /**
     * 
     * @return
     *     The commentsReceivedCount
     */
    public int getCommentsReceivedCount() {
        return commentsReceivedCount;
    }

    /**
     * 
     * @param commentsReceivedCount
     *     The comments_received_count
     */
    public void setCommentsReceivedCount(int commentsReceivedCount) {
        this.commentsReceivedCount = commentsReceivedCount;
    }

    /**
     * 
     * @return
     *     The followersCount
     */
    public int getFollowersCount() {
        return followersCount;
    }

    /**
     * 
     * @param followersCount
     *     The followers_count
     */
    public void setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
    }

    /**
     * 
     * @return
     *     The followingsCount
     */
    public int getFollowingsCount() {
        return followingsCount;
    }

    /**
     * 
     * @param followingsCount
     *     The followings_count
     */
    public void setFollowingsCount(int followingsCount) {
        this.followingsCount = followingsCount;
    }

    /**
     * 
     * @return
     *     The likesCount
     */
    public int getLikesCount() {
        return likesCount;
    }

    /**
     * 
     * @param likesCount
     *     The likes_count
     */
    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    /**
     * 
     * @return
     *     The likesReceivedCount
     */
    public int getLikesReceivedCount() {
        return likesReceivedCount;
    }

    /**
     * 
     * @param likesReceivedCount
     *     The likes_received_count
     */
    public void setLikesReceivedCount(int likesReceivedCount) {
        this.likesReceivedCount = likesReceivedCount;
    }

    /**
     * 
     * @return
     *     The projectsCount
     */
    public int getProjectsCount() {
        return projectsCount;
    }

    /**
     * 
     * @param projectsCount
     *     The projects_count
     */
    public void setProjectsCount(int projectsCount) {
        this.projectsCount = projectsCount;
    }

    /**
     * 
     * @return
     *     The reboundsReceivedCount
     */
    public int getReboundsReceivedCount() {
        return reboundsReceivedCount;
    }

    /**
     * 
     * @param reboundsReceivedCount
     *     The rebounds_received_count
     */
    public void setReboundsReceivedCount(int reboundsReceivedCount) {
        this.reboundsReceivedCount = reboundsReceivedCount;
    }

    /**
     * 
     * @return
     *     The shotsCount
     */
    public int getShotsCount() {
        return shotsCount;
    }

    /**
     * 
     * @param shotsCount
     *     The shots_count
     */
    public void setShotsCount(int shotsCount) {
        this.shotsCount = shotsCount;
    }

    /**
     * 
     * @return
     *     The teamsCount
     */
    public int getTeamsCount() {
        return teamsCount;
    }

    /**
     * 
     * @param teamsCount
     *     The teams_count
     */
    public void setTeamsCount(int teamsCount) {
        this.teamsCount = teamsCount;
    }

    /**
     * 
     * @return
     *     The canUploadShot
     */
    public boolean isCanUploadShot() {
        return canUploadShot;
    }

    /**
     * 
     * @param canUploadShot
     *     The can_upload_shot
     */
    public void setCanUploadShot(boolean canUploadShot) {
        this.canUploadShot = canUploadShot;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The pro
     */
    public boolean isPro() {
        return pro;
    }

    /**
     * 
     * @param pro
     *     The pro
     */
    public void setPro(boolean pro) {
        this.pro = pro;
    }

    /**
     * 
     * @return
     *     The bucketsUrl
     */
    public String getBucketsUrl() {
        return bucketsUrl;
    }

    /**
     * 
     * @param bucketsUrl
     *     The buckets_url
     */
    public void setBucketsUrl(String bucketsUrl) {
        this.bucketsUrl = bucketsUrl;
    }

    /**
     * 
     * @return
     *     The followersUrl
     */
    public String getFollowersUrl() {
        return followersUrl;
    }

    /**
     * 
     * @param followersUrl
     *     The followers_url
     */
    public void setFollowersUrl(String followersUrl) {
        this.followersUrl = followersUrl;
    }

    /**
     * 
     * @return
     *     The followingUrl
     */
    public String getFollowingUrl() {
        return followingUrl;
    }

    /**
     * 
     * @param followingUrl
     *     The following_url
     */
    public void setFollowingUrl(String followingUrl) {
        this.followingUrl = followingUrl;
    }

    /**
     * 
     * @return
     *     The likesUrl
     */
    public String getLikesUrl() {
        return likesUrl;
    }

    /**
     * 
     * @param likesUrl
     *     The likes_url
     */
    public void setLikesUrl(String likesUrl) {
        this.likesUrl = likesUrl;
    }

    /**
     * 
     * @return
     *     The projectsUrl
     */
    public String getProjectsUrl() {
        return projectsUrl;
    }

    /**
     * 
     * @param projectsUrl
     *     The projects_url
     */
    public void setProjectsUrl(String projectsUrl) {
        this.projectsUrl = projectsUrl;
    }

    /**
     * 
     * @return
     *     The shotsUrl
     */
    public String getShotsUrl() {
        return shotsUrl;
    }

    /**
     * 
     * @param shotsUrl
     *     The shots_url
     */
    public void setShotsUrl(String shotsUrl) {
        this.shotsUrl = shotsUrl;
    }

    /**
     * 
     * @return
     *     The teamsUrl
     */
    public String getTeamsUrl() {
        return teamsUrl;
    }

    /**
     * 
     * @param teamsUrl
     *     The teams_url
     */
    public void setTeamsUrl(String teamsUrl) {
        this.teamsUrl = teamsUrl;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.username);
        dest.writeString(this.htmlUrl);
        dest.writeString(this.avatarUrl);
        dest.writeString(this.bio);
        dest.writeString(this.location);
        dest.writeParcelable(this.links, flags);
        dest.writeInt(this.bucketsCount);
        dest.writeInt(this.commentsReceivedCount);
        dest.writeInt(this.followersCount);
        dest.writeInt(this.followingsCount);
        dest.writeInt(this.likesCount);
        dest.writeInt(this.likesReceivedCount);
        dest.writeInt(this.projectsCount);
        dest.writeInt(this.reboundsReceivedCount);
        dest.writeInt(this.shotsCount);
        dest.writeInt(this.teamsCount);
        dest.writeByte(this.canUploadShot ? (byte) 1 : (byte) 0);
        dest.writeString(this.type);
        dest.writeByte(this.pro ? (byte) 1 : (byte) 0);
        dest.writeString(this.bucketsUrl);
        dest.writeString(this.followersUrl);
        dest.writeString(this.followingUrl);
        dest.writeString(this.likesUrl);
        dest.writeString(this.projectsUrl);
        dest.writeString(this.shotsUrl);
        dest.writeString(this.teamsUrl);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.username = in.readString();
        this.htmlUrl = in.readString();
        this.avatarUrl = in.readString();
        this.bio = in.readString();
        this.location = in.readString();
        this.links = in.readParcelable(Links.class.getClassLoader());
        this.bucketsCount = in.readInt();
        this.commentsReceivedCount = in.readInt();
        this.followersCount = in.readInt();
        this.followingsCount = in.readInt();
        this.likesCount = in.readInt();
        this.likesReceivedCount = in.readInt();
        this.projectsCount = in.readInt();
        this.reboundsReceivedCount = in.readInt();
        this.shotsCount = in.readInt();
        this.teamsCount = in.readInt();
        this.canUploadShot = in.readByte() != 0;
        this.type = in.readString();
        this.pro = in.readByte() != 0;
        this.bucketsUrl = in.readString();
        this.followersUrl = in.readString();
        this.followingUrl = in.readString();
        this.likesUrl = in.readString();
        this.projectsUrl = in.readString();
        this.shotsUrl = in.readString();
        this.teamsUrl = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
