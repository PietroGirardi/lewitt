package com.lewitt.pietrogirardi.lewitt.model;

import android.util.Log;

import com.lewitt.pietrogirardi.lewitt.entities.Shot;
import com.lewitt.pietrogirardi.lewitt.interfaces.IModelShot;
import com.lewitt.pietrogirardi.lewitt.interfaces.IPresenterShot;
import com.lewitt.pietrogirardi.lewitt.interfaces.IShotAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by p.girardi on 6/30/2016.
 */

public class ModelShot implements IModelShot {

    public static final String ENDPOINT = "http://api.dribbble.com/v1/";
    public static final String TOKEN = "6288d1baba6d36c25f2b62037e644f450d6e8d2b8c68d064bae2adeed090d6d3";

    private IPresenterShot presenterShot;
    private List<Shot> listShot;

    public ModelShot(IPresenterShot presenterShot) {
        this.presenterShot = presenterShot;
    }

    @Override
    public void getListShotFromModel(int page) {

        if(listShot == null){
            listShot = new ArrayList<Shot>();
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IShotAPI api = retrofit.create(IShotAPI.class);

        // Create a call instance for looking up Retrofit contributors.
        Call<List<Shot>> call = api.getListShot(TOKEN,page);


        //if we don't want to/ need callback, we can use simple like this
        // call.execute().body()

        call.enqueue(new Callback<List<Shot>>() {
            @Override
            public void onResponse(Call<List<Shot>> call, Response<List<Shot>> response) {
                listShot.addAll(response.body());
                presenterShot.listShotResultFromModel(listShot);
            }

            @Override
            public void onFailure(Call<List<Shot>> call, Throwable t) {
                Log.e("LEWITT", "ERROR: "+t.getMessage());
                presenterShot.onFailure();
            }
        });
    }

    private boolean isListClear(){
        if(listShot== null || listShot.isEmpty()){
            return true;
        }
        return false;
    }

}
