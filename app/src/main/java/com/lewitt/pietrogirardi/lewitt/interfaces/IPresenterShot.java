package com.lewitt.pietrogirardi.lewitt.interfaces;

import android.app.Activity;

import com.lewitt.pietrogirardi.lewitt.entities.Shot;

import java.util.List;

/**
 * Created by p.girardi on 6/30/2016.
 */

public interface IPresenterShot {
    void getListShot(int page);
    void listShotResultFromModel(List<Shot> listShot);
    void onFailure();
    void onItemClick(Activity activity, Shot shot);
    void onDestroy();
}
