package com.lewitt.pietrogirardi.lewitt.interfaces;


import com.lewitt.pietrogirardi.lewitt.entities.Shot;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by p.girardi on 6/30/2016.
 */

public interface IShotAPI {

    @GET("shots")
    Call<List<Shot>> getListShot(@Query("access_token") String token, @Query("page") int page);
}
