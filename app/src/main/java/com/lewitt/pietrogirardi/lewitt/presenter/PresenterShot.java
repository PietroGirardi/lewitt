package com.lewitt.pietrogirardi.lewitt.presenter;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;

import com.bumptech.glide.Glide;
import com.lewitt.pietrogirardi.lewitt.R;
import com.lewitt.pietrogirardi.lewitt.entities.Shot;
import com.lewitt.pietrogirardi.lewitt.interfaces.IModelShot;
import com.lewitt.pietrogirardi.lewitt.interfaces.IPresenterShot;
import com.lewitt.pietrogirardi.lewitt.interfaces.IViewShot;
import com.lewitt.pietrogirardi.lewitt.model.ModelShot;
import com.lewitt.pietrogirardi.lewitt.view.ShotDetailFragmentView;

import java.util.List;

/**
 * Created by p.girardi on 6/30/2016.
 */

public class PresenterShot extends RecyclerView.OnScrollListener implements IPresenterShot {
    private IViewShot viewShot;
    private IModelShot modelShot;
    private ShotDetailFragmentView detailFragment;

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 2;
    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private int current_page = 0;

    public PresenterShot(IViewShot viewShot) {
        this.viewShot = viewShot;
    }


    @Override
    public void getListShot(int page) {
        if(modelShot == null){
            modelShot = new ModelShot(this);
        }
        modelShot.getListShotFromModel(page);
    }

    @Override
    public void listShotResultFromModel(List<Shot> listShot) {
        if(viewShot!= null)
            viewShot.onListShotResult(listShot);
    }

    @Override
    public void onFailure() {
        viewShot.onFailure();
    }

    @Override
    public void onItemClick(Activity activity, Shot shot) {

        if(activity.getResources().getBoolean(R.bool.isTablet)){
            detailFragment = (ShotDetailFragmentView) activity.getFragmentManager().findFragmentById(R.id.detail_fragment);
            detailFragment.updateShotDetail(shot);
        }else{
            detailFragment = new ShotDetailFragmentView();
            Bundle args = new Bundle();
            args.putParcelable(ShotDetailFragmentView.ARG_SHOT, shot);
            detailFragment.setArguments(args);

            createAnimation();
            FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, detailFragment);
            transaction.addToBackStack(null); //add the transaction to the back stack so the user can navigate back
            transaction.commit();
        }
    }

    @Override
    public void onDestroy() {
        viewShot = null;
    }

    private void createAnimation(){
        //TRANSITION ANIMATION
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Slide trans = new Slide();
            trans.setDuration(1000);

            detailFragment.setEnterTransition(trans);
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();

        totalItemCount = layoutManager.getItemCount();
        visibleItemCount = layoutManager.getChildCount();

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }

        if(layoutManager instanceof GridLayoutManager){
            firstVisibleItem = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
        }else{
            firstVisibleItem = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
        }

//        if (!loading && (totalItemCount - visibleItemCount)
//                <= (firstVisibleItem + visibleThreshold)) {

        if (!loading && (firstVisibleItem > totalItemCount-visibleThreshold)) {

            // End has been reached
            current_page++;
            if(current_page>=2){ //page one loads from shot fragment
                Log.i("TAG", "loading page "+current_page);
                modelShot.getListShotFromModel(current_page);
                loading = true;
            }
        }

    }
}
