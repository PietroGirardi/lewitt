package com.lewitt.pietrogirardi.lewitt.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lewitt.pietrogirardi.lewitt.R;
import com.lewitt.pietrogirardi.lewitt.entities.Shot;
import com.lewitt.pietrogirardi.lewitt.view.ShotFragmentView;
import com.peekandpop.shalskar.peekandpop.PeekAndPop;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by p.girardi on 6/30/2016.
 */

public class ShotAdapter extends RecyclerView.Adapter<ShotAdapter.MyViewHolder> {

    private List<Shot> listShot;
    private LayoutInflater lInflater;
    OnItemClickListener OnClickListenerHack;
    Context context;
    PeekAndPop peekAndPop;
    private ImageView imgPeek;
    private TextView textTitle;
    private TextView txtViewsCountPeek;

    public ShotAdapter(Context context, List<Shot> listShot, PeekAndPop peekAndPop) {
        this.context = context;
        this.listShot = listShot;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.peekAndPop = peekAndPop;

        setupPeekAndPop();
    }


    private void setupPeekAndPop() {
        imgPeek = (ImageView) peekAndPop.getPeekView().findViewById(R.id.img_shot_peek);
        textTitle = (TextView) peekAndPop.getPeekView().findViewById(R.id.txt_title_peek);
        txtViewsCountPeek = (TextView) peekAndPop.getPeekView().findViewById(R.id.txt_views_count_peek);

        //callback of PeekAndPop
        this.peekAndPop.setOnGeneralActionListener(new PeekAndPop.OnGeneralActionListener() {
            @Override
            public void onPeek(View view, int position) { //peek
                loadPeekAndPop(position);
            }

            @Override
            public void onPop(View view, int position) { //pop
            }
        });
    }

    private void loadPeekAndPop(int position) {
        Glide.with(context)
                .load(listShot.get(position).getImages().getNormal())
                //.placeholder(R.drawable.placeholder)
                //.error(R.drawable.xxx)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .fitCenter()
                .centerCrop()
                .into(imgPeek);

        textTitle.setText(listShot.get(position).getTitle());
        txtViewsCountPeek.setText(String.valueOf(listShot.get(position).getViewsCount()));
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        if(ShotFragmentView.layoutManagerType == ShotFragmentView.LAYOUT_MANAGER_LIST){
            view = lInflater.inflate(R.layout.item_shot, parent, false);
        }else{
            view = lInflater.inflate(R.layout.item_shot_grid, parent, false);
        }

        MyViewHolder mvh = new MyViewHolder(view);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        peekAndPop.addLongClickView(holder.itemView, position);

        String URL = listShot.get(position).getImages().getNormal();

        //// TODO: 30/06/16 estudy about diskCache Glide
        Glide.with(context)
                .load(URL)
                //.placeholder(R.drawable.placeholder)
                //.error(R.drawable.XXXX)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .fitCenter()
                .centerCrop()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.imgShot);

        //holder.txtTitleShot.setText(listShot.get(position).getTitle());
        //holder.txtViewsShot.setText(String.valueOf(listShot.get(position).getViewsCount()));

        if(holder.textNameAvatar!=null){

            Glide.with(context)
                    .load(listShot.get(position).getUser().getAvatarUrl())
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .fitCenter()
                    .centerCrop()
                    .into(holder.circleImageViewAvatar);

            holder.textNameAvatar.setText(listShot.get(position).getUser().getName());
            holder.textImageTitle.setText(listShot.get(position).getTitle());
        }
    }

    @Override
    public int getItemCount() {
        if (listShot != null) {
            return listShot.size();
        }
        return 0;
    }

    public void setRecyclerViewOnClickListenerHack(OnItemClickListener rv) {
        OnClickListenerHack = rv;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.img_shot) ImageView imgShot;
        @BindView(R.id.progress_bar) ProgressBar progressBar;
        TextView textNameAvatar;
        TextView textImageTitle;
        CircleImageView circleImageViewAvatar;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            if(isUsingList(itemView)){
                circleImageViewAvatar = ButterKnife.findById(itemView, R.id.img_avatar);
                textNameAvatar = ButterKnife.findById(itemView, R.id.txt_avatar_name);
                textImageTitle = ButterKnife.findById(itemView, R.id.txt_img_title);
            }
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (OnClickListenerHack != null) {
                OnClickListenerHack.onItemClick(listShot.get(getAdapterPosition()));
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Shot shot);
    }

    private boolean isUsingList(View itemView){
        return itemView.findViewById(R.id.img_avatar) != null; //img_avatar just exist in listview
    }
}